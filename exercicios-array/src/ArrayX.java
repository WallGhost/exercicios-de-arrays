import java.util.Scanner;

public class ArrayX {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de Vari�veis
		int[] x = new int[10];
		
		//Preenchendo o Array
		for(int i = 0; i < x.length; i++) {
			x[i] = 30;
		}
		
		//Exibindo o Array
		for(int i = 0; i < x.length; i++) {
			System.out.println("�ndice[" + i + "]: " + x[i]);
		}
		
	}

}
