import java.util.Scanner;
public class ArrayXeY {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de Vari�veis
		int X [] = new int [10];
		int Y [] = new int [10];
		
		for(int i = 0; i < X.length; i++) {
			System.out.println("Digite um valor:");
			X[i] = teclado.nextInt();
		}
		
		for(int i = 0; i < Y.length; i++) {
			if(i % 2 == 0) {
				Y[i] = X[i] / 2;
			}else {
				Y[i] = X[i] * 3;
			}
		}
		
		//Exibindo o Vetor
		for(int i = 0; i < X.length; i++){
			System.out.println("Vetor X [" + i + "]: " + X[i]);
		}
		
		for(int i = 0; i< Y.length; i++){
			System.out.println("Vetor Y [" + i + "]: " + Y[i]);
		}

	}

}
