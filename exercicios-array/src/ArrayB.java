import java.util.Scanner;

public class ArrayB {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		int B [] = new int [10];
		
		//Preenchendo o vetor B
		for(int i = 0; i < B.length; i++) {
			if(i % 2 == 0) {
				B[i] = 0;
			}else {
				B[i] = 1;
			}
		}
		
		//Exibindo o Vetor
		for(int i = 0; i< B.length; i++){
			System.out.println("�ndice[" + i + "]: " + B[i]);
		}
		
	}

}
