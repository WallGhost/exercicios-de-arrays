import java.util.Scanner;

public class ArrayC {

	public static void main(String[] args) {
		Scanner teclado = new Scanner(System.in);
		
		//Declara��o de Vari�veis
		int C [] = new int [10];
		
		//Preenchendo o Vetor
		for(int i = 0; i < C.length; i++) {
			System.out.println("Digite um valor:");
			C[i] = teclado.nextInt();
			
			if(C[i] < 0) {
				C[i] = 0;
			}
		}
		
		//Exibindo o Vetor
		for(int i = 0; i< C.length; i++){
			System.out.println("�ndice[" + i + "]: " + C[i]);
		}

	}

}
